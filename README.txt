BlockAdBlock Integration
========================

Exposes https://github.com/sitexw/BlockAdBlock plugin through the Libraries
API. This is intended to be used by other modules or by any custom code you
write.

Usage
=====

Recommended usage is in the #attached property of a render array:

<?php
/**
 * Implements hook_preprocess_html().
 */
function mymodule_preprocess_html(&$variables) {
  $variables['page']['page_bottom']['#attached']['libraries_load'][]
    = array('blockadblock');
}
?>

Alternate usage is via libraries_load():

<?php
/**
 * Some function in your custom module.
 */
function mymodule_some_function() {
  libraries_load('blockadblock');
}
?>

For the JavaScript side, please refer to the documentation for the original
plugin:
https://github.com/sitexw/BlockAdBlock/blob/v3.2.0/README.md#code-example

Notes
=====

- The library file is set to the footer scope, so it'll
  load as part of the script tags right before the closing body tag.
