core = 7.x
api = 2

; Download v3.2.0
libraries[blockadblock][download][type]     = "git"
libraries[blockadblock][download][url]      = "https://github.com/sitexw/BlockAdBlock.git"
libraries[blockadblock][directory_name]     = "blockadblock"
libraries[blockadblock][destination]        = "libraries"
libraries[blockadblock][download][revision] = "c01bcfcc5261c558b2dd05604f7a74305bf7f07f"
